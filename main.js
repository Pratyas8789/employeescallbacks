const { error, log } = require('console');
const fs = require('fs');
const path = require('path');

function retrieve(findIds, callback) {
    fs.readFile('./data.json', 'utf8', (error, data) => {
        if (error) {
            callback(error, null);
            return;
        }
        else {
            try {
                data = (JSON.parse(data));
            } catch (error) {
                callback(error, null);
                return;
            }
            let retrieveData = data["employees"].filter((employeeData) => (findIds).includes(employeeData.id));
            try {
                retrieveData = JSON.stringify(retrieveData, null, 4);
            } catch (error) {
                callback(error, null);
                return;
            }
            fs.writeFile(path.join('./output', "retrieveData.json"), retrieveData, (error) => {
                if (error) {
                    callback(error, null);
                    return;
                } else {
                    callback(null, data);
                    return;
                }
            })
        }
    });
    return;
}

function groupDataBasedOnCompanies(data, callback) {
    let groupDataBasedOnCompanies = data["employees"].reduce((acc, employeeData) => {
        if (employeeData["company"] === "Scooby Doo") {
            acc["Scooby Doo"].push(employeeData);
        } else if (employeeData["company"] === "Powerpuff Brigade") {
            acc["Powerpuff Brigade"].push(employeeData);
        } else if (employeeData["company"] === "X-Men") {
            acc["X-Men"].push(employeeData);
        }
        return acc;
    }, {
        "Scooby Doo": [],
        "Powerpuff Brigade": [],
        "X-Men": []
    });
    try {
        groupDataBasedOnCompanies = JSON.stringify(groupDataBasedOnCompanies, null, 4);
    } catch (error) {
        callback(error, null);
        return;
    }
    fs.writeFile(path.join('./output', "groupDataBasedOnCompanies.json"), groupDataBasedOnCompanies, (error) => {
        if (error) {
            callback(error, null);
            return;
        } else {
            callback(null, data);
            return;
        }
    });
    return;
}

function powerpuffBrigadeCompanyData(data, callback) {
    let powerpuffBrigadeCompanyData = data["employees"].filter((employeeData) => (employeeData.company === "Powerpuff Brigade"))
    try {
        powerpuffBrigadeCompanyData = JSON.stringify(powerpuffBrigadeCompanyData, null, 4);
    } catch (error) {
        callback(error, null);
        return;
    }
    fs.writeFile(path.join('./output', "powerpuffBrigadeCompanyData.json"), powerpuffBrigadeCompanyData, (error) => {
        if (error) {
            callback(error, null);
            return;
        } else {
            callback(null, data);
            return;
        }
    });
    return;
}

function removeEntry(data, callback) {
    let removeEntry = data["employees"].filter((employeeData) => (employeeData.id !== 2))
    try {
        removeEntry = JSON.stringify(removeEntry, null, 4);
    } catch (error) {
        callback(error, null);
        return;
    }
    fs.writeFile(path.join('./output', "removeEntry.json"), removeEntry, (error) => {
        if (error) {
            callback(error, null);
            return;
        } else {
            callback(null, data);
            return;
        }
    });
    return;
}

function sortData(data, callback) {
    let sortedData = data["employees"].sort((employee1, employee2) => {
        if (employee1.company === employee2.company) {
            if (employee1.id < employee2.id) {
                return -1;
            }
        }
        else if (employee1.company < employee2.company) {
            return -1;
        }
    });
    try {
        sortedData = JSON.stringify(sortedData, null, 4);
    } catch (error) {
        callback(error, null);
        return;
    }
    fs.writeFile(path.join('./output', "sortedData.json"), sortedData, (error) => {
        if (error) {
            callback(error, null);
            return;
        } else {
            callback(null, data);
            return;
        }
    });
    return;
}

function swapPosition(data, callback) {
    const dataWhichId92 = data["employees"].filter((ele) => ele.id === 92);
    const dataWhichId93 = data["employees"].filter((ele) => ele.id === 93);
    let companyWhichId92 = dataWhichId92[0]["company"];
    let companyWhichId93 = dataWhichId93[0]["company"];

    let newResult = data.employees.map(obj => {
        if (obj.id === 92) {
            obj.company = companyWhichId93;
        }
        if (obj.id === 93) {
            obj.company = companyWhichId92;
        }
        return obj;
    });

    try {
        newResult = JSON.stringify(newResult, null, 4);
    } catch (error) {
        callback(error, null);
        return;
    }
    fs.writeFile(path.join('./output', "swapPosition.json"), newResult, (error) => {
        if (error) {
            callback(error, null);
            return;
        } else {
            callback(null, data);
            return;
        }
    });
    return;
}

function addBirthday(data, callback) {
    let addBirthday = data.employees.filter((currentEmployee) => {
        if (currentEmployee.id % 2 === 0) {
            currentEmployee.birthday = new Date().toString().substring(4, 15);
        }
        return currentEmployee;
    });
    try {
        addBirthday = JSON.stringify(addBirthday, null, 4);
    } catch (error) {
        callback(error, null);
        return;
    }
    fs.writeFile(path.join('./output', 'addBirthday.json'), addBirthday, (error) => {
        if (error) {
            callback(error, null);
            return;
        } else {
            callback(null, "All task done successfully!!!");
            return;
        }
    });
    return;
}


retrieve([2, 13, 23], (error, data) => {
    if (error) {
        console.error(error);
    } else {
        groupDataBasedOnCompanies(data, (error, data) => {
            if (error) {
                console.error(error);
            } else {
                powerpuffBrigadeCompanyData(data, (error, data) => {
                    if (error) {
                        console.error(error);
                    } else {
                        removeEntry(data, (error, data) => {
                            if (error) {
                                console.error(error);
                            } else {
                                sortData(data, (error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        swapPosition(data, (error, data) => {
                                            if (error) {
                                                console.error(error);
                                            } else {
                                                addBirthday(data, (error, data) => {
                                                    if (error) {
                                                        console.error(error);
                                                    } else {
                                                        console.log(data);
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
})